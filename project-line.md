
## basic commands

### basic install
python -m django --version
django-admin startproject silensus-djg
python manage.py migrate
python manage.py runserver
python manage.py startapp blog

### orm 
-- after models creation 
-- 'blog.apps.BlogConfig', change on settings.py
-- apply migrations
python manage.py makemigrations
python manage.py migrate

### user
python manage.py createsuperuser

### /admin
-- admin.site.register dans admin.py
from .models import Post - for first model Post
admin.site.register(Post)


from django.urls import path
from . import views
# from .views import post_detail  # Importez votre vue post_detail

app_name = 'blog'

urlpatterns = [
    path('', views.post_list, name='post_list'),
    path('<slug:slug>/', views.post_detail, name='post_detail'),
]

    # path('sitemap.xml', views.sitemap, name='sitemap'),
    # path('robots.txt', views.robots, name='robots'),
    # path('googlee3c7e3b2e1f5c0d8.html', views.google, name='google'),
    # path('BingSiteAuth.xml', views.bing, name='bing'),
    # path('Yandex_9b5d0a3c4c6a5d1d.html', views.yandex, name='yandex'),
    # path('sitemap.xml.gz', views.sitemap_gz, name='sitemap_gz'),
    # path('sitemap.xml.gz.txt', views.sitemap_gz_txt, name='sitemap_gz_txt'),
    # path('sitemap.xml.txt', views.sitemap_txt, name='sitemap_txt'),
    # path('sitemap.xml.zip', views.sitemap_zip, name='sitemap_zip'),
    # path('sitemap.xml.zip.txt', views.sitemap_zip_txt, name='sitemap_zip_txt'),
    # path('sitemap.xml.xz', views.sitemap_xz, name='sitemap_xz'),
    # path('sitemap.xml.xz.txt', views.sitemap_xz_txt, name='sitemap_xz_txt'),
    # path('sitemap.xml.bz2', views.sitemap_bz2, name='sitemap_bz2'),
    # path('sitemap.xml.bz2.txt', views.sitemap_bz2_txt, name='sitemap_bz2_txt'),
    # path('sitemap.xml.br', views.sitemap_br, name='sitemap_br'),

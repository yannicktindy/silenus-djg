from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Post

# Create your views here.
def post_list(request):
    object_list = Post.objects.all()
    paginator = Paginator(object_list, 1) # 2 posts in each page
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        posts = paginator.page(paginator.num_pages)
    context = {
        'posts': posts,
        'page': page,
    }    
    return render(request, 'blog/post/list.html', {'posts': posts})

def post_detail(request, slug):
    post = Post.objects.get(slug=slug)
    # post = get_object_or_404(Post, slug=slug)
    return render(request, 'blog/post/detail.html', {'post': post}) 

    # try:
    #     post = Post.objects.get(slug=slug)
    # except Post.DoesNotExist:
    #     raise Http404("Post does not exist")
    # return render(request, 'blog/post/detail.html', {'post': post})



